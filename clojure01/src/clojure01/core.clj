(ns clojure01.core)

;; <  Menor que
;; 1. Verifica si el número es negativo
(defn función-menor-que-1
  [n]
  (< n 0))
;; 2. Devuelve el menor de dos números
(defn función-menor-que-2
  [a b]
  (if (< a b) a b))
;; 3. Verifica si es menor de edad
(defn función-menor-que-3
  [n]
  (< n 18))

(función-menor-que-1 34)
(función-menor-que-2 56 1)
(función-menor-que-3 23)



;; <= Menor o igual
;; 1. Verifica si la suma de un par de números es <= que la multiplicación
(defn función-menor-igual-1
  [a b]
  (<= (+ a b) (* a b)))
;; 2. Verifica que el año de nacimiento sea correcto
(defn función-menor-igual-2
  [n]
  (if (<= n 2020) "Año válido" "Año inválido"))
;; 3. Verifica si el número es menor o igual a 100
(defn función-menor-igual-3
  [n]
  (<= n 100))

(función-menor-igual-1 23 32)
(función-menor-igual-2 2023)
(función-menor-igual-3 100)



;; == Igual
;; 1. Verifica si la suma es igual a la multiplicación
(defn función-igual-1
  [a b]
  (== (+ a b) (* a b)))
;; 2. Demuestra la igualdad matemática de la suma
(defn función-igual-2
  [a b]
  (== (+ a b) (+ b a)))
;; 3. Demuestra la igualdad matemática de la resta
(defn función-igual-3
  [a b]
  (== (- a b) (- b a)))

(función-igual-1 43 65)
(función-igual-2 34 67)
(función-igual-3 44 56)



;; >  Mayor que
;; 1. Verifica si el número es positivo
(defn función-mayor-que-1
  [n]
  (> n 0))
;; 2. Verifica si el número es mayor que 100
(defn función-mayor-que-2
  [n]
  (> n 100))
;; 3. Devuelve el mayor de dos números
(defn función-mayor-que-3
  [a b]
  (if (> a b) a b))

(función-mayor-que-1 98)
(función-mayor-que-2 45)
(función-mayor-que-3 45 98)



;; >= Mayor o igual
;; 1. Verifica si es mayor de edad
(defn función-mayor-igual-1
  [n]
  (if (>= n 18) "Es mayor de edad" "Es menor de edad"))
;; 2. Verifica si el número es mayor o igual que 100
(defn función-mayor-igual-2
  [n]
  ((>= n 100)))
;; 3. Verifica si un a es mayor o igual respecto a b
(defn función-mayor-igual-3
  [a b]
  (>= a b))

(función-mayor-igual-1 18)
(función-mayor-igual-2 456)
(función-mayor-igual-3 24 53)



;; assoc Asociacion
;; 1. Sustituye con b en la posicion a
(defn función-asociación-1
  [a b]
  (assoc [1 2 3 4] a b))
;; 2. Agrega la edad a la credencial del alumno
(defn función-asociación-2
  [n]
  (assoc {} :nombre "Alejandra" :carrera "ISC" :edad n))
;; 3. Le da simbolo al número Tres
(defn función-asociación-3
  [n]
  (assoc {} :Uno 1 :Dos 2 :Tres n))

(función-asociación-1 0 3)
(función-asociación-2 23)
(función-asociación-3 3)



;; concat Concatenacion
;; 1. Agrega otro número
(defn función-concatenación-1
  [n]
  (concat [1 4 2 5] [n]))
;; 2. Agrega otros dos números
(defn función-concatenación-2
  [a b]
  (concat [6 3 2 8] [a b]))
;; 3. Agrega otros tres números
(defn función-concatenación-3
  [a b c]
  (concat [7 3 4 5] [a b c]))

(función-concatenación-1 9)
(función-concatenación-2 4 6)
(función-concatenación-3 0 1 2)



;; conj
;; 1. Agrega un elemento más al vector
(defn función-conj-1
  [n]
  (conj [1 2 3 4] n))
;; 2. Agrega un elemento más a la lista
(defn función-conj-2
  [n]
  (conj '(1 2 3 4) n))
;; 3. Agrega un par de elementos más a la lista
(defn función-conj-3
  [n m]
  (conj '(1 2 3 4) n m))

(función-conj-1 12)
(función-conj-2 90)
(función-conj-3 10 20)



;; contains? Contiene
;; 1. Verifica si un vector tiene cierto número
(defn función-contiene-1
  [n]
  (contains? [1 2 3 4] n))
;; 2. Verifica si un mapa contiene cierto número
(defn función-contiene-2
  [n]
  (contains? #{0 9 8 7} n))
;; 3. Verifica si un elemento se encuentra en un vector de vectores
(defn función-contiene-3
  [n]
  (contains? [[1 2 3] [4 5 6] [7 8 9]] n))

(función-contiene-1 6)
(función-contiene-2 0)
(función-contiene-3 8)



;; count Contador
;; 1. Cuenta los elementos
(defn función-contador-1
  [n]
  (count n))
;; 2. Verifica si n es el número de elementos
(defn función-contador-2
  [n]
  (== (count [1 2 3 4]) n))
;; 3. Cuenta si el vector 1 es más grande que el vector 2
(defn función-contador-3
  [n m]
  (> (count n) (count m)))

(función-contador-1 [1 2 3 4])
(función-contador-2 4)
(función-contador-3 [1 2 3 4] [5 6 7 8 9])



;; disj 
;; 1. Busca el elemento y lo elimina
(defn función-disj-1
  [n]
  (disj #{1 2 3 4 5 6} n))
;; 2. Busca la palabra y la elimina
(defn función-disj-2
  [n]
  (disj #{"Hola" "Adios" "Hasta luego"} n))
;; 3. Busca el elemento en un mapa de dos tipos de elementos y lo elimina
(defn función-disj-3
  [n]
  (disj #{"a" "b" "c" "d" 1 2 3 4} n))

(función-disj-1 4)
(función-disj-2 "Adios")
(función-disj-3 2)